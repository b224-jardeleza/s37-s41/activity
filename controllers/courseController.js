const Course = require("../models/Course");
const User = require("../models/User");

// Controller Functions:
// Creating a new course
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false //"Course creation failed"
		} else {
			return true
		}
	})
};


// Retrieving All Courses
module.exports.getAllCourses = (data) => {

	if(data.isAdmin){
		return Course.find({}).then(result => {
			
			return result
		})
	} else {
		return false // "You are not an admin."
	}
};


// Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result => {
		return result
	})
};


// Retrieve a Specific Course
module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {

		return result
	})
};


// Update a Specific Course
module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {

		console.log(updatedCourse)
		if(error){
			return false
		} else {
			return true
		}
	})
};


// Archive a course - Solution #1
module.exports.archiveCourse = (reqParams, reqBody) => {

	let archivedCourse = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((archivedCourse, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
};


/*
// Archive a course - Solution #2
module.exports.archiveCourse = (reqParams, reqBody) => {

	return Course.findById(reqParams).then(result => {

		if(result.isAdmin == false){
			let archivedCourse = {
				isActive: true
			}

		} else {

			let archivedCourse = {
				isActive: false
			}
		}
		
		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((archivedCourse, error) => {

			if(error){
				return false
			} else {
				return true
			}
		})
		
	})
};
*/

